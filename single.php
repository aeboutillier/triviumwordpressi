<?php get_header(); ?>
    <main>
    <?php while( have_posts() ) : the_post(); ?>
        <article class="singleBox">
            <div class="leftPost">
                <h1><?php the_title(); ?></h1>
                <p><?php the_content(); ?></p> 
            </div>
            
            <div class="rightPost">
                <div class="imgPost">
                    <?php the_post_thumbnail('medium'); ?>
                </div>
                
                <?php if ( !function_exists('get_field')) return; ?>
                <ul>
                    <li>
                        <h3>Taille : <?php the_field('taille'); ?> (toise)</h3>
                    </li>
                    
                    <li>
                        <h3>Régime :</h3>
                        <?php
                        $animaux = get_field('regime');
                        if( $animaux ): ?>
                        <ul>  
                            <?php foreach( $animaux as $post ): 
                            // Setup this post for WP functions (variable must be named $post).
                            setup_postdata($post); ?>
                            
                            <li>
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            </li>
                            <?php endforeach; ?>
                        </ul>  
                        <?php 
                        // Reset the global post object so that the rest of the page works correctly.
                        wp_reset_postdata(); ?>
                        <?php endif; ?>
                    </li>
                </ul>
            </div>
        </article>
    <?php endwhile; ?>
    </main>
<?php get_footer(); ?>