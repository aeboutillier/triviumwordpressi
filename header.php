<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="apple-touch-icon" sizes="180x180" href="/wordpress/wp-content/themes/roxxor/assets/images/favicon_io/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/wordpress/wp-content/themes/roxxor/assets/images/favicon_io/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/wordpress/wp-content/themes/roxxor/assets/images/favicon_io/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous"> -->
    <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>">
    <title><?php bloginfo('name'); ?></title>

    <?php wp_head(); ?>

</head>
<body>
    <!-- <?php wp_nav_menu( array( 'theme_location' => 'menu_principal' ) ); ?> -->
    <header>
        <div id="pandaBox">
            <div class="imgBan">
                <img id="ban" src="https://www.especes-menacees.fr/wp-content/uploads/2021/07/panda-geant-quitte-liste-animaux-menaces-chine.jpg" alt="logo">
            </div>
            <a class="textBan" href="/wordpress/index.php">
                <h1>Bestiaire</h1>
                <h2>Index</h2>
            </a>
        </div>
        <div id="deco">
            <img id="tree" src="/wordpress/wp-content/themes/roxxor/assets/images/tree-branch-transparent.png" alt="Tree" />
        </div>
    </header>
    