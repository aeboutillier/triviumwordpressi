
<?php get_header() ?>

<main>

    <?php 
    $lettreB = "";
    // $B la lettre dans laquelle on est pour l'instant 
   
    while(have_posts()) : the_post();

        $lettreA = substr(get_the_title(), 0, 1);
        // $A = la premiere lettre de l'animal 
        if($lettreA !== $lettreB) {  // si A !== B

            $lettreB = $lettreA; // B = A
            ?>

            <h2 class="categorie"><?php echo $lettreB ?></h2>

            <?php   // echo h2 B
            }
            ?>
        
            <article class="articleBox">
                <div class="articleContent">
                    <a title="Voir l'article" href="<?php the_permalink(); ?>">
                        <h3><?php the_title(); ?></h3>
                        <p><?php the_excerpt(); ?></p>
                    </a>
                </div>
            </article>
    <?php endwhile; ?>

</main>

<?php get_footer() ?>