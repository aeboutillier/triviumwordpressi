<?php


function wpb_custom_query( $query ) {

    // Make sure we only modify the main query on the homepage
    if( $query->is_main_query() && ! is_admin() && $query->is_home() ) {
    
        // Set parameters to modify the query
        $query->set( 'orderby', 'title' );
        $query->set( 'order', 'ASC' );
    }
}

    
// Hook our custom query function to the pre_get_posts 
add_action( 'pre_get_posts', 'wpb_custom_query' );

add_theme_support('post-thumbnails');

// -----------------------------------------------------------------------

if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array(
        'key' => 'group_62274fec51832',
        'title' => 'Details',
        'fields' => array(
            array(
                'key' => 'field_62275299e5b94',
                'label' => 'Taille',
                'name' => 'taille',
                'type' => 'number',
                'instructions' => '',
                'required' => 1,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => 'Toise',
                'min' => '',
                'max' => '',
                'step' => '',
            ),
            array(
                'key' => 'field_622753bee5b95',
                'label' => 'Régime',
                'name' => 'regime',
                'type' => 'relationship',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'post_type' => '',
                'taxonomy' => '',
                'filters' => array(
                    0 => 'search',
                    1 => 'post_type',
                    2 => 'taxonomy',
                ),
                'elements' => '',
                'min' => '',
                'max' => '',
                'return_format' => 'object',
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'post',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
        'show_in_rest' => 0,
    ));
    
    endif;		

?>